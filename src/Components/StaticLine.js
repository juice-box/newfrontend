import React from 'react';
import {Line} from 'react-chartjs-2';

var createReactClass = require('create-react-class');

const INSTRUMENTS = [
	{ name: "Astronomica", price: ["94303"] },
	{ name: "Borealis", price: ["94088"] },
	{ name: "Celestial", price: ["95062"] },
	{ name: "Deuteronic", price: ["96803"] },
	{ name: "Eclipse", price: ["94303"] },
	{ name: "Floral", price: ["94088"] },
	{ name: "Galactia", price: ["95062"] },
	{ name: "Heliosphere", price: ["96803"] },
	{ name: "Interstella", price: ["94303"] },
	{ name: "Jupiter", price: ["94088"] },
	{ name: "Koronis", price: ["95062"] },
	{ name: "Lunatic", price: ["96803"] }
  ];

const data = {
  labels: ["Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse",
  "Floral", "Galactia", "Heliosphere", "Interstella", "Jupiter", "Koronis", "Lunatic"],
  datasets: [
    {
      label: 'historical trading data',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(75,192,192,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [13726, 21449, 84272,20204,24941,21008, 63976,69730, 13447, 93453, 32375, 61742]
    }
  ]
};

export default createReactClass({
  displayName: 'Line',

  render() {
    return (

      
      <div>
        <h2>historical trading data</h2>
        <Line data={data} />
      </div>
    );
  }
});